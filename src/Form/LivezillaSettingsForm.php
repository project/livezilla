<?php

namespace Drupal\livezilla\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class LivezillaSettingsForm.
 */
class LivezillaSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'livezilla.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'livezilla_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('livezilla.settings');
    $form['chat_widget_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Chat Widget ID'),
      '#description' => $this->t('ID attribute value for the chat widget.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('chat_widget_id'),
    ];
    $form['chat_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Chat Domain'),
      '#description' => $this->t('The domain for your chat server without a scheme specified (e.g. chat.example.com).'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('chat_domain'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('livezilla.settings')
      ->set('chat_widget_id', $form_state->getValue('chat_widget_id'))
      ->set('chat_domain', $form_state->getValue('chat_domain'))
      ->save();
  }

}
