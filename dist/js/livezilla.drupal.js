(function () {
  'use strict';

  (function ($, Drupal, drupalSettings) {
    var targetNode = document.head;
    var config = {
      attributes: false,
      childList: true,
      subtree: false
    };
    var callback = function callback(mutationsList, observer) {
      $("#livezilla_wm").attr("tabindex", 0);
      $("#livezilla_wm").keydown(function (event) {
        var keycode = event.keyCode || event.which;
        if (keycode === 13) {
          $("#livezilla_wm").click();
        }
      });
      if (typeof $("#livezilla_wm").attr("tabindex") !== "undefined") {
        observer.disconnect();
      }
    };
    var observer = new MutationObserver(callback);
    observer.observe(targetNode, config);
    Drupal.behaviors.livezilla = {
      attach: function attach() {
        drupalSettings.livezilla.human = false;
        document.body.addEventListener("mousemove", function () {
          Drupal.livezilla.initChat();
        });
        document.body.addEventListener("touchmove", function () {
          Drupal.livezilla.initChat();
        });
        document.body.addEventListener("keydown", function (e) {
          if (e.code === "Tab" || e.code === "Enter") {
            Drupal.livezilla.initChat();
          }
        });
      }
    };
    Drupal.livezilla.initChat = function () {
      if (!drupalSettings.livezilla.human) {
        $("body").append("<script id=\"".concat(drupalSettings.livezilla.chat_widget_id, "\" src=\"//").concat(drupalSettings.livezilla.chat_domain, "/script.php?id='").concat(drupalSettings.livezilla.chat_domain, "'\"></script>"));
      }
      drupalSettings.livezilla.human = true;
    };
  })(jQuery, Drupal, drupalSettings);

})();
