# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.0.0](https://git.dropfort.com/dropfort/dropfort_module_build/compare/v1.0.1...v2.0.0) (2023-06-07)


### Bug Fixes

* **tag:** remove v from release tag ([c8dfb85](https://git.dropfort.com/dropfort/dropfort_module_build/commit/c8dfb85d3d33d16d73d16f99a9b3496bad3a12ff))

### [1.0.1](https://git.dropfort.com/dropfort/dropfort_module_build/compare/v1.0.0-alpha.5...v1.0.1) (2023-06-07)


### Code Refactoring

* **build:** upgrade to rollup ([bcb0e3e](https://git.dropfort.com/dropfort/dropfort_module_build/commit/bcb0e3eeb84a823ec140d12881f69607cf470549))
* **d9:** allow drupal9 ([b7ec451](https://git.dropfort.com/dropfort/dropfort_module_build/commit/b7ec45132733858185b2ebf007d33618a83dda36))
* **js:** add antibot pattern ([1b2e354](https://git.dropfort.com/dropfort/dropfort_module_build/commit/1b2e354d54f68ba1007cddfa66b49da2d644a3ca))

## [1.0.0-alpha.5](https://git.dropfort.com///compare/v1.0.0-alpha.4...v1.0.0-alpha.5) (2020-03-20)


### Bug Fixes

* **permissions:** use different base permission ([d3a3e18](https://git.dropfort.com///commit/d3a3e1877869a52dd6ec7cbdc7a5b7ffd9a9360c))

## [1.0.0-alpha.4](https://git.dropfort.com///compare/v1.0.0-alpha.3...v1.0.0-alpha.4) (2020-03-03)


### Bug Fixes

* **attributes:** adjust load order ([dc960da](https://git.dropfort.com///commit/dc960da9da304a03798601cc0566a29f1404770d))

## [1.0.0-alpha.3](https://git.dropfort.com///compare/v1.0.0-alpha.2...v1.0.0-alpha.3) (2020-03-03)


### Code Refactoring

* **access:** remove anonymous restriction ([939a9e2](https://git.dropfort.com///commit/939a9e21ba92afb0d8d6782f579e1b1764efd7fb))

## [1.0.0-alpha.2](https://git.dropfort.com///compare/v1.0.0-alpha.1...v1.0.0-alpha.2) (2020-03-03)


### Features

* **a11y:** add keyboard trigger on widget ([3bd3063](https://git.dropfort.com///commit/3bd3063bf4629e91c218f06580966ca21d8e7542))
* **a11y:** add keyboard trigger on widget ([2a206ce](https://git.dropfort.com///commit/2a206ce08e3f75fd1d23534bc4bf829489c749b8))
* **a11y:** add keyboard trigger on widget ([afab167](https://git.dropfort.com///commit/afab1675031ba9f231bcc46bf176dc7c292ec748))


### Bug Fixes

* **paths:** update to the new dist dir ([b16d780](https://git.dropfort.com///commit/b16d780a56d9f93bd2456aeac78dd1f3a4f88760))

## 1.0.0-alpha.1 (2020-02-18)


### Features

* **release:** add packaging files ([99c8b2d](https://git.dropfort.com///commit/99c8b2dca95e72a1be506411dd490cbc21f27b72))


### Bug Fixes

* **js:** place in footer ([a6e5f88](https://git.dropfort.com///commit/a6e5f882aa38b66affe6cf8b848f9419b465bb19))

## 1.0.0-alpha.1 (2020-02-18)


### Bug Fixes

* **js:** place in footer ([a6e5f88](https://git.dropfort.com///commit/a6e5f882aa38b66affe6cf8b848f9419b465bb19))

## 1.0.0-alpha.1 (2020-02-18)


### Bug Fixes

* **js:** place in footer ([a6e5f88](https://git.dropfort.com///commit/a6e5f882aa38b66affe6cf8b848f9419b465bb19))
