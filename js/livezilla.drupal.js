(($, Drupal, drupalSettings) => {
  // Select the node that will be observed for mutations
  const targetNode = document.head;

  // Options for the observer (which mutations to observe)
  const config = {
    attributes: false,
    childList: true,
    subtree: false,
  };

  const callback = (mutationsList, observer) => {
    // Allow the widget trigger to be keyboard navigable.
    $("#livezilla_wm").attr("tabindex", 0);
    $("#livezilla_wm").keydown(event => {
      const keycode = event.keyCode || event.which;
      if (keycode === 13) {
        $("#livezilla_wm").click();
      }
    });

    // Disconnect once we're set it.
    if (typeof $("#livezilla_wm").attr("tabindex") !== "undefined") {
      observer.disconnect();
    }
  };

  // Create an observer instance linked to the callback function
  const observer = new MutationObserver(callback);

  // Start observing the target node for configured mutations
  observer.observe(targetNode, config);

  Drupal.behaviors.livezilla = {
    attach() {
      // Assume the user is not human, despite JS being enabled.
      drupalSettings.livezilla.human = false;

      // Wait for a mouse to move, indicating they are human.
      document.body.addEventListener("mousemove", () => {
        // Unlock the forms.
        Drupal.livezilla.initChat();
      });

      // Wait for a touch move event, indicating that they are human.
      document.body.addEventListener("touchmove", () => {
        // Unlock the forms.
        Drupal.livezilla.initChat();
      });

      // A tab or enter key pressed can also indicate they are human.
      document.body.addEventListener("keydown", e => {
        if (e.code === "Tab" || e.code === "Enter") {
          // Unlock the forms.
          Drupal.livezilla.initChat();
        }
      });
    },
  };

  /**
   * Unlock all locked forms.
   */
  Drupal.livezilla.initChat = () => {
    // Act only if we haven't yet verified this user as being human.
    if (!drupalSettings.livezilla.human) {
      $("body").append(
        `<script id="${drupalSettings.livezilla.chat_widget_id}" src="//${drupalSettings.livezilla.chat_domain}/script.php?id='${drupalSettings.livezilla.chat_domain}'"></script>`
      );
    }

    // Mark this user as being human.
    drupalSettings.livezilla.human = true;
  };
})(jQuery, Drupal, drupalSettings);
